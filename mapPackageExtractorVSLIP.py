#-------------------------------------------------------------------------------
# Name:        extract SLIP Map packaes
# Purpose:
#
# Author:      Michael pemberton
#              Original code from landgate and modified for Python 3 environment
# Created:     19/11/2019
#
#-------------------------------------------------------------------------------
import json
import requests
import shutil
from urllib.parse import urlparse
import config
import arcpy
import os

def getSecrets():

    # Secrets from file stored outside of revison control

    with open(r"./secrets.json") as f:

        secrets = json.load(f)

    return secrets

def fetchDownloadSnapshot(URL):
    response = s.get(URL, allow_redirects=False, stream=True)
    if response.status_code == 302:
        parsed_uri = urlparse(response.headers['Location'])
        domain = "{uri.scheme}://{uri.netloc}/".format(uri=parsed_uri)
        if parsed_uri.netloc.startswith("sso.slip.wa.gov.au") or parsed_uri.netloc.startswith("maps.slip.wa.gov.au"):
            response = fetchDownloadSnapshot(response.headers['Location'])
        else:
            raise Exception("Receieved a redirect to an unknown domain '%s' for %s" % (parsed_uri.netloc, response.headers['Location']))
    if response.status_code == 200:
        if response.headers["Content-Type"] == "application/zip" or response.headers["Content-Type"] == "application/octet-stream":
            return response
        raise Exception("Received an invalid Content-Type response - should be 'application/zip' or 'application/octet-stream', but was '{}'".format(response.headers["Content-Type"]))
    else:
        raise Exception("Received a '%s' response for the URL %s" % (response.status_code, URL))

def extractPackage(filetoextract,directory_name):
    arcpy.env.overwriteOutput = True
    arcpy.ExtractPackage_management(filetoextract,config.packages_directory)

# Secrets
secrets = getSecrets()
s = requests.Session()
s.auth = (secrets["username"], secrets["password"])
s.headers.update({"User-Agent": "SLIPAppUser"})

def process_files():

    x = datetime.datetime.now().strtime('%Y/%m/%d %H:%M')

    for urllocation in config.URLlist:
        #response = fetchDownloadSnapshot(urllocation)
        filename = urllocation.split('/')[-1]
        with open(config.download_directory+'\\'+filename, mode="wb") as localfile:
           shutil.copyfileobj(response.raw, localfile)
        response.close()
        os.mkdir(config.packages_directory+'\\'+filename.split('.')[0])
        arcpy.ExtractPackage_management(config.download_directory+'\\'+filename, config.packages_directory+'\\'+filename.split('.')[0])

def main():
    #arcpy.SignInToPortal(arcpy.GetActivePortalURL(), 'username', 'password')
    process_files()

if __name__ == '__main__':
    main()
