# README #

The following document is to help you implement the SLIP map package downloader.

### What is this repository for? ###

* This Python application connects to SLIP and downloads a series of map packages as layed out in the config.py file
* Version 1.0

### How do I get set up? ###

* This application requires a working directory to place the files in and two directories:  download_directory and packages_directory
* You will need a Version of ArtcGIS Pro loaded and Pythen 3.6 or higher
* You will require an account for this application to run under in your Portal account with access to python
* No database configuration is needed, but you will need to supply a list of map packages you want downloaded in the config.py file
* Deployment instructions:  copy the secrets.json, config.py and mapPackageExtractorVSLIP.py to a location where the windows scheduler can get to and the account to run the script has permissions.
