#-------------------------------------------------------------------------------
# Name:        config
# Purpose:
#
# Author:      Michael Pemberton
# Company:     Esri Australia
#
# Created:     19/11/2019
#-------------------------------------------------------------------------------

download_directory = '<directory to put downloaded map packages>\downloads'
packages_directory = '<directory to extract map packages to>\packages'

#example of map packagesd.  More then one can be added and must be seperated by commas
URLlist = {'https://maps.slip.wa.gov.au/datadownloads/Landgate_v2_Subscription_Services/Tenure/Tenure.mpk','https://maps.slip.wa.gov.au/datadownloads/Landgate_v2_Subscription_Services/Cadastral/Cadastral.mpk'}
