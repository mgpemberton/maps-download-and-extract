The purpose of this code is to download packages from SLIP and then extract the map packages.  The code can be either run from pyscripter or Python engine and can also be called from a scheduler system for automation purposes.

The secrets json file is where you would place the account user and password to access the SLIP data.

depending how you implement the code, it may be necessary to have the python code login to Portal for licencing purposes.

arcpy.SignInToPortal(arcpy.GetActivePortalURL(), 'username', 'password') has been commented out in the main section of the code.

The following directoies need to be created.  They exist in the config file.  You can create your own, but then you need to change the following settings:

download_directory = '<location to put map package(s)>\downloads'
packages_directory = '<location to extract packages to>\packages'